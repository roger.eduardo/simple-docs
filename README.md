# Simple Docs

This is just a dummy repo to validate a few documentation assumptions

## Can gitlab show plantuml diagrams

```plantuml
@startuml
  Alice -> Bob: Authentication Request
  Bob --> Alice: Authentication Response

  Alice -> Bob: Another authentication Request
  Alice <-- Bob: Another authentication Response
@enduml
```
